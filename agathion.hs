{-# LANGUAGE OverloadedStrings #-}
module Main where

import Agathion.Git
import Agathion.Twitter
import Agathion.Weather

import Brick
import Brick.AttrMap
import Brick.BChan
import Brick.Types
import Brick.Widgets.Border
import Brick.Widgets.Border.Style
import Brick.Widgets.Center
import Control.Concurrent
import Control.Monad
import Control.Monad.IO.Class
import Data.List.Split
import Data.Time
import Graphics.Vty
import System.Directory
import System.Process (readProcess)
import System.Random
import Text.Read (readMaybe)

data State = State { weather :: Maybe Weather
                   , uptime :: String
                   , host :: String
                   , kernel :: String
                   , bits :: String
                   , ram :: String
                   , drives :: String
                   , img :: [String]
                   , time :: UTCTime
                   , timeZone :: TimeZone
                   , repos :: [Repo] 
                   , tweets :: [Tweet]
                   , cal :: String
                   , quote :: String
                   , options :: [Option]
                   } 
  deriving (Show)

data Option = OptionRepos [FilePath]
            | OptionWeather String
            | OptionTwitter [String]
            | OptionGCal Bool
            | OptionQuote FilePath
            | OptionInterval Int
  deriving (Show, Eq)

stringToOption :: String -> [Option]
stringToOption s = case fmap (drop 1) . break (=='=') $ s of
  ("twitter",ts)  -> [OptionTwitter $ splitOn "," ts]
  ("git",rs)      -> [OptionRepos $ splitOn "," rs]
  ("location",l)  -> [OptionWeather l]
  ("gcal",n)      -> [OptionGCal (n=="1" || n=="true" || n=="True")]
  ("quotes",q)    -> [OptionQuote q]
  ("interval",i)  -> case readMaybe i :: Maybe Int of
                      (Just i') -> [OptionInterval i']
                      _         -> []
  _               -> []

getIntervalFromOptions :: [Option] -> Int
getIntervalFromOptions [] = 5
getIntervalFromOptions (x:xs) = case x of
  (OptionInterval i)  -> i
  _                   -> getIntervalFromOptions xs

--This is used to mark time passing
data Step = Step
  deriving (Ord, Eq)

drawUI :: State -> [Widget Step]
drawUI st = [padTop (Pad 1) $ vBox [(boxDiag <+> boxQuote <+> boxWeather)
                                   ,boxImg <+> (boxTwitter <=> 
                                               (boxRepoDay <+> boxRepoWeek) <=> 
                                               boxGCal)
                                   ]
            ]
  where boxDiag = let titleBar = hBox [withAttr "blue" $ str $ host st,str $ " ("++kernel st++", "++bits st++"-bit)"]
                      box = padBottom (Pad 1) . padLeftRight 2 . withBorderStyle ascii . borderWithLabel titleBar . padAll 1
                  in vLimit 15 $ box $ hBox [fill ' ',str $ drives st,fill ' ',str $ ram st,fill ' ']
        boxQuote = if (quote st)==""
                   then emptyWidget
                   else hLimit 40 $ vLimit 15 $ makeBox Nothing . bottom $ strWrap $ quote st
        boxWeather = hLimit 50 $ vLimit 15 $ makeBox (Just "Weather") . bottom $ weatherWidget $ weather st
        boxTwitter = makeBox (Just "Twitter") . left $ tweetWidget $ tweets st
        boxRepoDay = let reposToday = map (\(t,cs) -> (t,filter (commitToday (time st) (timeZone st)) cs)) $ repos st
                     in makeBox (Just "Today") . left $ vBox $ 
                        map (\r -> (repoTitle r) <=> (commitData True $ snd r)) $ reposToday
        boxRepoWeek = makeBox (Just "This Week") . left $ vBox $ 
                        map (\r -> (repoTitle r) <=> (commitData False $ snd r)) $ repos st
        boxImg = let i = textWidth $ (img st)!!0
                 in hLimit (i+(i `div` 2)) $ 
                      makeBox Nothing $ vBox . map hCenter $ [(withAttr "red" $ str "Uptime: ") <+> (str $ uptime st)
                                                             ,fill ' ']
                                                             ++(map (\x -> hCenter $ withAttr "blue" $ str x) (img st))
                                                             ++[fill ' ']
        boxGCal = if (OptionGCal True) `elem` options st
                  then makeBox (Just "gcalcli") . hCenter $ str $ cal st
                  else emptyWidget

makeBox :: Maybe String -> Widget n -> Widget n
makeBox (Just x) = padBottom (Pad 1) . padLeftRight 2 . withBorderStyle ascii . borderWithLabel (str x) . padAll 1
makeBox Nothing = padBottom (Pad 1) . padLeftRight 2 . withBorderStyle ascii . border . padAll 1

left :: Widget n -> Widget n
left = flip (<+>) (fill ' ')

bottom :: Widget n -> Widget n
bottom = flip (<=>) (fill ' ')

chooseCursor :: State -> [CursorLocation Step] -> Maybe (CursorLocation Step)
chooseCursor st _ = Nothing

handleEvent :: State -> BrickEvent Step e -> EventM Step (Next State)
handleEvent st (VtyEvent ev) = case ev of
  EvKey KEsc []         -> halt st
  EvKey (KChar 'q') []  -> halt st
  EvKey (KChar 'r') []  -> suspendAndResume $ updateState True st
  EvKey (KChar 'R') []  -> suspendAndResume $ updateState False st
  _                     -> continue st
handleEvent st _ = suspendAndResume $ updateState True st

updateState :: Bool -> State -> IO State
updateState fork s = do
  dataPath <- getAppUserDataDirectory "agathion"

  let refresh = mapM_ (refreshOption dataPath) $ options s
  if fork then (forkIO refresh) >> return () else refresh

  reposExist <- doesFileExist $ dataPath++"/.git.tmp"
  newRepos <- if reposExist
              then do
                copyFile (dataPath++"/.git.tmp") (dataPath++"/.git")
                f <- readFile $ dataPath++"/.git"
                return (read f :: [Repo])
              else return []

  tweetsExist <- doesFileExist $ dataPath++"/.twitter.tmp"
  newTweets <- if tweetsExist
               then do
                 copyFile (dataPath++"/.twitter.tmp") (dataPath++"/.twitter")
                 f <- readFile $ dataPath++"/.twitter"
                 return (read f :: [Tweet])
               else return []

  weatherExists <- doesFileExist $ dataPath++"/.weather.tmp"
  newWeather <- if weatherExists
                then do
                  copyFile (dataPath++"/.weather.tmp") (dataPath++"/.weather")
                  f <- readFile $ dataPath++"/.weather"
                  return (read f :: Maybe Weather)
                else return Nothing

  calExists <- doesFileExist $ dataPath++"/.gcal.tmp"
  newCal <- if calExists
            then do
              copyFile (dataPath++"/.gcal.tmp") (dataPath++"/.gcal")
              return =<< readFile $ dataPath++"/.gcal"
            else return ""

  quoteExists <- doesFileExist $ dataPath++"/.quote.tmp"
  newQuote <- if quoteExists
              then do
                copyFile (dataPath++"/.quote.tmp") (dataPath++"/.quote")
                return =<< readFile $ dataPath++"/.quote"
              else return ""

  newUptime <- readProcess "uptime" [] ""
  newHostname <- readProcess "hostname" [] ""
  newKernel <- readProcess "uname" ["-r"] ""
  newBits <- readProcess "uname" ["-m"] ""
  newRam <- readProcess "free" ["-mht"] ""
  newDrives <- readProcess "df" ["-h"] ""
  newTime <- getCurrentTime
  newZone <- getTimeZone newTime

  return $ s { weather = newWeather
             , uptime = let ws = words newUptime
                        in if (length ws>1) && (length (ws!!2)>1) then init $ ws!!2 else newUptime
             , host = init newHostname
             , kernel = init newKernel
             , bits = init newBits
             , ram = newRam
             , drives = newDrives
             , time = newTime
             , timeZone = newZone
             , repos = newRepos
             , tweets = newTweets
             , cal = newCal
             , quote = newQuote
             }

refreshOption :: FilePath -> Option -> IO ()
refreshOption dataPath (OptionRepos fps) = do
  newCommits <- mapM (\x -> withCurrentDirectory x $ readProcess "git" ["log","--since=\"7 days ago\""] "") fps
  writeFile (dataPath++"/.git.tmp") $ show $ zip fps (map stringToCommits newCommits)
refreshOption dataPath (OptionWeather w) = writeFile (dataPath++"/.weather.tmp") =<< showIO =<< getWUnderground w
refreshOption dataPath (OptionTwitter t) = writeFile (dataPath++"/.twitter.tmp") =<< showIO =<< getTweets t
refreshOption dataPath (OptionGCal b) = when b $ writeFile (dataPath++"/.gcal.tmp") =<< readProcess "gcalcli" ["agenda"] ""
refreshOption dataPath (OptionQuote q) = writeFile (dataPath++"/.quote.tmp") =<< chooseLine q
refreshOption dataPath _ = return ()

showIO :: Show a => a -> IO String
showIO a = return (show a)

chooseLine :: FilePath -> IO String
chooseLine fp = do
  file <- readFile fp
  if (length file)>0
  then do
    r <- randomRIO (0,(length $ lines file)-1)
    return $ (lines file)!!r
  else return $ "Error reading "++fp

theMap :: AttrMap
theMap = attrMap defAttr [(attrName "red", fg red)
                         ,(attrName "blue", fg blue)
                         ,(attrName "green", fg green)
                         ]

main :: IO ()
main = do
  dataPath <- getAppUserDataDirectory "agathion"

  configExists <- doesFileExist $ dataPath++"/config"
  opts <- if configExists
          then do
            f <- readFile $ dataPath++"/config"
            return $ concatMap stringToOption $ splitOn "\n" f
          else return [OptionTwitter ["inspire_us"]]
  newTime <- getCurrentTime
  newZone <- getTimeZone newTime

  imgExists <- doesFileExist $ dataPath++"/img"
  imgF <- if imgExists
          then do
            f <- readFile $ dataPath++"/img"
            return $ splitOn "\n" f
          else return [""]

  initialState <- updateState False $ State Nothing "" "" "" "" "" "" imgF newTime newZone [] [] "" "" opts

  let app = App { appDraw = drawUI
                , appChooseCursor = chooseCursor
                , appHandleEvent = handleEvent
                , appStartEvent = return
                , appAttrMap = const theMap
                }
  chan <- newBChan 1
  forkIO $ forever $ do
    writeBChan chan Step
    threadDelay $ 60000000*(getIntervalFromOptions opts)
  vty <- mkVty defaultConfig
  finalState <- customMain vty (mkVty defaultConfig) (Just chan) app initialState

  mapM_ (\x -> do
    exists <- doesFileExist $ dataPath++"/"++x
    if exists then removeFile $ dataPath++"/"++x else return ()
    ) [".weather",".weather.tmp",".twitter",".twitter.tmp",".git",".git.tmp",".quote",".quote.tmp",".gcal",".gcal.tmp"]
